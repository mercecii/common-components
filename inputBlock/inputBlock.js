import React from 'react';
import { Form, Button } from 'react-bootstrap';
import './inputBlock.css';

export default function InputBlock(props) {
    const {label, type, placeholder
    } = props;

    return (
        <div className="input-block">
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>{label}</Form.Label>
                <Form.Control type={type} placeholder={placeholder} />
            </Form.Group>

            
        </div>
    );
}
