import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { ButtonBlock, InputBlock } from '..';
import './formBlock.css'

export default function FormBlock(props) {
    const {} = props;

    return (
        <Form className="form-block">
            {/* <InputBlock 
                label="Email"
                type="email"
                placeholder="Enter your Email"
                inputClass=""
            />
            <InputBlock 
                label="Password"
                type="password"
                placeholder="Enter your Password"
                btnClass=""
            />
            <ButtonBlock
                label="Submit"

            /> */}
            {props.children}
        </Form>
    );
}
