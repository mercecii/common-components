import React from 'react';
import {Button} from 'react-bootstrap';
import './buttonBlock.css';


export default function ButtonBlock(props) {
    const {label, btnclassName
    } = props;

    return (
        <div className={btnclassName? btnclassName: ' '}>
            <Button variant="primary" type="submit">
                {label}
            </Button>
        </div>
    );

}